import java.util.*;

public class Methods {


    public static void getRequirements() 
    {
        System.out.println("Sphere Volume Program");
        System.out.println();
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches,");
        System.out.println("and rounds to two decimal places.");
        System.out.println("Must use Java's *built-in* Pi and pow() capabilities");
        System.out.println("Program checks for non-integers and non numberic values.");
        System.out.println("Progra, continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.");
        System.out.println();
    }
    
    public static void getSphereVolume()
    {
        int diameter = 0;
        double volume = 0.0;
        double gallons = 0.0;
        char choice = ' ';
        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Please enter diameter in inches: ");
            while (!sc.hasNextInt())
            {
                System.out.println("Not Valid Integer\n");
                sc.next();
                System.out.print("Please try again. Enter diameter in inches: ");
            }
            diameter = sc.nextInt();
            System.out.println();

            volume = ((4.0/3.0) * Math.PI * Math.pow(diameter/2.0, 3));
            gallons = volume/231;
            System.out.println("Sphere volume: " + String.format("%,.2f", gallons) + " liquid U.S. gallons");

            System.out.print("\nDo you want to calculate another sphere volume (y or n)? ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        System.out.println("\nThank you for using our Sphere Volume Calculator!");
    }
}