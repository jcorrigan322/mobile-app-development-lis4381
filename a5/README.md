# LIS 4381

## John Corrigan

### Assignment #5 Requirements:

1. Server side Validation
2. Complete skillsets 13-15
3. both server and client side validation working

#### README.md file should include the following items:

* Course title, name, and requirements
* Screenshots of database displayed in web app
* Screenshots of add to database app


#### Assignment Screenshots:

*Screenshot of a5 index.php*:
![a5 index.php](img/index.png)

*Screenshot of invalid add a petstore*:
<table><tr>
<td> <img src="img/add-petstore-invalid.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/failed-validation.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>

*Screenshot of valid add a petstore*:

<table><tr>
<td> <img src="img/add-petstore-valid.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/passed-validation.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>

*Screenshot of simple calculator skillset*:

<table><tr>
<td> <img src="img/calc1-input.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/calc1-output.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>
<table><tr>
<td> <img src="img/calc2-input.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/calc2-output.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>

*Screenshot of read/write file skillset*:

<table><tr>
<td> <img src="img/read.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/write.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>



*Screenshot of SKillset 13 Sphere Volume*:
![Skillset 13 Sphere Volume calculator](img/ss13.png)

