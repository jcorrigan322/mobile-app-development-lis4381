# LIS 4381

## John Corrigan

### Project #2 Requirements:

1. Add edit/update functionality
2. Delete functionality
3. RSS feed under test tab

#### README.md file should include the following items:

* Course title, name, and requirements
* Update failed then succeeded
* delete prompt and success


#### Assignment Screenshots:

*Screenshot of a5 index.php*:
![P2 index.php](img/petstore.png)

*Screenshot of updating a petstore*:
<table><tr>
<td> <img src="img/failed-validation.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/passed-validation.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>

*Screenshot of deleteing a petstore*:

<table><tr>
<td> <img src="img/delete-prompt.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/deleted.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>

*Screenshot of Nasa RSS feed*:

![RSS-Feed](img/rss-feed.png)